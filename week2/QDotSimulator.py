#from mlqe2022_public.day_2.CapacitanceQuantumDotArray import CapacitanceQuantumDotArray
import itertools
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

class DoubleDot():

    def __init__(self):
        super().__init__

        ###############################################################
        # user changeable variables
        ###############################################################


        self.voltRangeMin = 20.0
        self.voltRangeMax = -60.0
        self.voltsPerAxis = 100
        self.neighborCapacitanceFactor = np.random.uniform(0.2,2)
        self.selfCapacitanceFactor = 1 + np.random.exponential(7)
        self.how_noisy = 0.3
        self.window_size = 10
        self.cmap = 'binary'

        ###############################################################
        # previously (fixed) defined variables
        ###############################################################

        self.numDots = 2
        self.numGates = 2
        dotDotMutualCapacitanceMatrix = np.array([[1,1],[1,1]])
        gateDotMutualCapacitanceMatrix = np.array([[1,1],[1,1]])
        physicalDotLocations = [(-0.5,0),(0.5,0)]
        self.dotAdjacencyMatrix = None
        self.whichDotsAreOhmicConnected = None
        self.there_is_window = False

        #in meV, from paper
        U = 6.1 #/ meVPerJoule
        U12 = 2.5 #/ meVPerJoule

        alpha = U / (U + U12)
        beta = 1 - alpha
        gamma = U/2


        vMin = self.voltRangeMin
        vMax = self.voltRangeMax
        self.numVoltagesPerAxis = self.voltsPerAxis

        self.numProbePoints = self.numVoltagesPerAxis ** 2
        self.voltageArr = np.zeros(shape=(self.numProbePoints,self.numGates))

        VL = np.linspace(vMin,vMax,self.numVoltagesPerAxis)
        VR = np.linspace(vMin,vMax,self.numVoltagesPerAxis)

        mu1 = alpha*VL + beta*VR + gamma
        mu2 = beta*VL + alpha*VR + gamma

        #derived these relations myself for a symmetric dot
        self.Cg = 1/(U+U12) #* (unitCharge**2)
        self.Cm = U12 / (U**2-U12**2) #* (unitCharge**2)

        cSig2 = self.Cg**2 + 2*self.Cg*self.Cm

        Ec11 = 1/cSig2 * (self.Cg+self.Cm)
        Ec22 = 1/cSig2 * (self.Cg+self.Cm)
        Ecm = 1/cSig2 * (self.Cm)

        E_C_Yang = np.array([[Ec11,Ecm],[Ecm,Ec22]])
        self.voltageArr = [ [m1,m2] for m1 in VL for m2 in VR]
        self.voltageArr = np.array(self.voltageArr)
        #selfcapacitance is given; I derived neighbor capacitance
        #unitCharge converts

        self.Cg = self.Cg/2 #correction for gate bullshit
    

    def reset_capacitances(self, neighborC = np.random.uniform(0.2,1), selfC = 0.2+np.random.exponential(7)):
        self.neighborCapacitanceFactor = neighborC
        self.selfCapacitanceFactor = selfC


    def generate_csd(self):

        selfCapacitance = self.Cg*self.selfCapacitanceFactor
        neighborCapacitance = self.Cm*self.neighborCapacitanceFactor


        dotDotMutualCapacitanceMatrix = np.eye( self.numDots ) * selfCapacitance
        dotDotMutualCapacitanceMatrix[0,1] = neighborCapacitance
        dotDotMutualCapacitanceMatrix[1,0] = neighborCapacitance


        gateDotMutualCapacitanceMatrix = np.zeros([self.numDots, self.numGates] )

        #gate 0 = V_L
        gateDotMutualCapacitanceMatrix[0,0] = self.Cg
        gateDotMutualCapacitanceMatrix[1,0] = 0
        gateDotMutualCapacitanceMatrix[1,1] = self.Cg
        gateDotMutualCapacitanceMatrix[0,1] = 0

        #
        physicalDotLocations =[[0,0],[1,0]]


        myQDArr = CapacitanceQuantumDotArray(dotDotMutualCapacitanceMatrix, gateDotMutualCapacitanceMatrix, physicalDotLocations, self.dotAdjacencyMatrix, self.whichDotsAreOhmicConnected)

        occupationArr, energyArr = myQDArr.probeManyVoltagesInARow(self.voltageArr)

        sensorCoordinates = (0,1)
        potentialArr = [0]*self.numProbePoints

        for lv in range(0,self.numProbePoints):
            currentOccupation = occupationArr[lv]
            potentialArr[lv] = myQDArr.sensePotential(currentOccupation, sensorCoordinates)

        potentialArr = np.reshape(potentialArr,(self.numVoltagesPerAxis,self.numVoltagesPerAxis))
        potentialArr = np.flipud(potentialArr)
        self.Z = potentialArr
        self.occupationArr = np.reshape(occupationArr,(self.numVoltagesPerAxis,self.numVoltagesPerAxis,2))

        dcharge = np.gradient(self.Z)
        dcharge_noisy = np.zeros((self.Z.shape[0], self.Z.shape[1]))

        dcharge0max = dcharge[0].max()
        dcharge1max = dcharge[1].max()



        dcharge0_noisy = abs(dcharge[0]) + self.how_noisy * dcharge0max * np.random.normal(size = (dcharge[0].shape[0], dcharge[0].shape[1]))
        dcharge1_noisy = abs(dcharge[1]) + self.how_noisy * dcharge1max * np.random.normal(size = (dcharge[1].shape[0], dcharge[1].shape[1]))

        self.dcharge_noisy = dcharge0_noisy + dcharge1_noisy
        self.dcharge = abs(dcharge[0]) + abs(dcharge[1])

    def import_csd(self, dcharge, dcharge_noisy):
        self.dcharge = dcharge
        self.dcharge_noisy = dcharge_noisy   


    def sample_window(self, random = True, show = False, save = False):

        self.there_is_window = True

        if random:
            # take a random index (int) from the minimum value of where Vx is plotted to the maximum value - the window size
            self.window_xmin = np.random.randint(self.voltsPerAxis - self.window_size)
            # do the same thing for y
            self.window_ymin = np.random.randint(self.voltsPerAxis - self.window_size)

        self.window_xmax = self.window_xmin + self.window_size
        self.window_ymax = self.window_ymin + self.window_size

        self.window_noiseless = self.dcharge[100 - self.window_xmax: 100 - self.window_xmin, self.window_ymin:self.window_ymax][::-1,:].T
        self.window =  self.dcharge_noisy[100 - self.window_xmax:100 - self.window_xmin, self.window_ymin:self.window_ymax][::-1,:].T
        plt.imshow(self.dcharge[self.window_xmin:self.window_xmax, self.window_ymin:self.window_ymax], cmap = self.cmap)
        if show: plt.show()
        plt.close()

    def plot_csd(self, noisy = True, with_window = False, show = False, save = False):

        number_of_subplots = 1 + noisy

        fig = plt.figure(figsize = (10*number_of_subplots, 11))
        ax = fig.add_subplot(int(f"1{number_of_subplots}1"))
        plt.imshow(self.dcharge[::-1], cmap = self.cmap)
        plt.xlim(0,self.voltsPerAxis-1)
        plt.ylim(0,self.voltsPerAxis-1)

        realTicks = np.linspace(0,self.voltsPerAxis-1,6)
        fakeVoltageTicks = [f"{item}" for item in np.linspace(self.voltageArr.T[0][0], self.voltageArr.T[0][-1], 6)]      
        
        plt.xticks(realTicks, fakeVoltageTicks, fontsize = 20)
        plt.yticks(realTicks, fakeVoltageTicks, fontsize = 20)
        plt.title('Noiseless', fontsize = 30)
        plt.xlabel('Gate Voltage 1', fontsize = 26)
        plt.ylabel('Gate Voltage 2', fontsize = 26)
        for spine in ['top', 'right', 'bottom', 'left']:
            ax.spines[spine].set_linewidth(3)
        ax.xaxis.set_tick_params(width=3)
        ax.yaxis.set_tick_params(width=3)

        if with_window and self.there_is_window:
            for window_pixel in range(self.window_size + 1):
                plt.scatter(self.window_xmin + window_pixel, self.window_ymin, c = 'r', marker = 's', s = 85)
                plt.scatter(self.window_xmin + window_pixel, self.window_ymax, c = 'r', marker = 's', s = 85)
                plt.scatter(self.window_xmin, self.window_ymin + window_pixel, c = 'r', marker = 's', s = 85)
                plt.scatter(self.window_xmax, self.window_ymin + window_pixel, c = 'r', marker = 's', s = 85)
            
        if noisy:
            ax = fig.add_subplot(int(f"1{number_of_subplots}2")) 
            plt.imshow(self.dcharge_noisy[::-1], cmap = self.cmap)
            plt.xlim(0,self.voltsPerAxis-1)
            plt.ylim(0,self.voltsPerAxis-1)
            plt.xticks(realTicks, fakeVoltageTicks, fontsize = 20)
            plt.yticks(realTicks, fakeVoltageTicks, fontsize = 20)
            plt.title('With noise', fontsize = 30)
            plt.xlabel('Gate Voltage 1', fontsize = 26)
            plt.ylabel('Gate Voltage 2', fontsize = 26)
            for spine in ['top', 'right', 'bottom', 'left']:
                ax.spines[spine].set_linewidth(3)
            ax.xaxis.set_tick_params(width=3)
            ax.yaxis.set_tick_params(width=3)


            if with_window and self.there_is_window:
                for window_pixel in range(self.window_size + 1):
                    plt.scatter(self.window_xmin + window_pixel, self.window_ymin, c = 'r', marker = 's', s = 85)
                    plt.scatter(self.window_xmin + window_pixel, self.window_ymax, c = 'r', marker = 's', s = 85)
                    plt.scatter(self.window_xmin, self.window_ymin + window_pixel, c = 'r', marker = 's', s = 85)
                    plt.scatter(self.window_xmax, self.window_ymin + window_pixel, c = 'r', marker = 's', s = 85)


        plt.tight_layout()
        self.csd_plot = fig
        if save: plt.savefig('example_csd.jpg', dpi = 200)
        if show: plt.show()
        self.fig = fig
        plt.close()

